FROM nixos/nix

ADD murmur.ini /etc/murmur.ini
ADD start-murmur.sh /start-murmur.sh

RUN addgroup -S murmur && adduser -S -G murmur murmur && \
	nix-env -f '<nixpkgs>' -iA murmur tini && \
	chmod +x /start-murmur.sh && \
	mkdir -p /murmur && chown -R murmur:murmur /murmur

EXPOSE 50051 64738 64738/udp 6502

ENTRYPOINT ["tini", "--"]
CMD ["/start-murmur.sh"]
