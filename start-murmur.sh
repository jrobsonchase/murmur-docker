#!/usr/bin/env sh

[ -f /murmur/murmur.ini ] || cp /etc/murmur.ini /murmur/

chown -R murmur:murmur /murmur

exec murmurd -v -ini /murmur/murmur.ini -fg
